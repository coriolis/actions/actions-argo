# actions Argo, gliders, bouées
Ce projet concerne les actions à traiter pour les données flotteurs Argo, gliders et bouées.

**Documentation, métadonnées**
- Les fiches de calibrations de gliders: \\iota1\coriolis\gestion\exploitation\glider\deploiement
- Les déploiements Argo: \\iota1\coriolis\gestion\exploitation\argo\flotteurs-coriolis
- Les informations bouées dérivantes: \\iota1\coriolis\gestion\exploitation\bouees

